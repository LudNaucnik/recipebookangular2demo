import { Component, OnInit } from '@angular/core';
import { RecipeService } from './recipes/recipe.service';
import { AuthService } from './shared/auth.service';

@Component({
  selector: 'rb-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  constructor(private recipeService: RecipeService, private auth: AuthService) { }

  ngOnInit() {
  }

  onStore() {
    this.recipeService.storeData()
  }

  onFetch() {
    this.recipeService.fetchData();
  }

  isAuth() {
    return this.auth.isAuthenticated();
  }

  onLogOut(){
    this.auth.logOut();
  }

}
