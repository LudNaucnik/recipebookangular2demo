import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../shared/auth.service';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'rb-sign-up',
  templateUrl: './sign-up.component.html',
  styles: []
})
export class SignUpComponent implements OnInit, OnDestroy {

  signUpForm: FormGroup;
  authErrorMessage = '';
  error = false;
  errorMessage = '';
  subscription: Subscription;

  constructor(private fb: FormBuilder, private auth: AuthService) {
  }

  onSignup() {
    this.auth.singUpUser(this.signUpForm.value);
  }

  ngOnInit(): any {
    this.subscription = this.auth.errorEmited.subscribe(
      (error: string) => this.authErrorMessage = error
    );
    this.signUpForm = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        this.isEmail
      ])],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', Validators.compose([
        Validators.required,
        this.isEqualPassword.bind(this)
      ])],
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  isEmail(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      return { noEmail: true };
    }
  }

  isEqualPassword(control: FormControl): { [s: string]: boolean } {
    if (!this.signUpForm) {
      return { passwordsNotMatch: true };

    }
    if (control.value !== this.signUpForm.controls['password'].value) {
      return { passwordsNotMatch: true };
    }
  }


}
