import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../shared/auth.service';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'rb-sign-in',
  templateUrl: './sign-in.component.html',
  styles: []
})
export class SignInComponent implements OnInit {

  signInForm: FormGroup;
  error = false;
  authErrorMessage = '';
  errorMessage = '';
  subscription: Subscription;

  constructor(private formBuilder: FormBuilder, private auth: AuthService) { }

  onSignin() {
    this.auth.singInUser(this.signInForm.value);
  }

  ngOnInit(): any {
    this.subscription = this.auth.errorEmited.subscribe(
      (error: string) => this.authErrorMessage = error
    );
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
