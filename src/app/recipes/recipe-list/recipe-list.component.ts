import { Component, OnInit, OnDestroy } from '@angular/core';
import { Recipe } from '../recipe';
import { RecipeService } from "../recipe.service";
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'rb-recipe-list',
  templateUrl: './recipe-list.component.html'
})
export class RecipeListComponent implements OnInit, OnDestroy {

  recipes: Recipe[] = [];
  subscription: Subscription;

  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
    this.recipeService.fetchData();
    this.subscription = this.recipeService.recipeChanged.subscribe(
      (recipes: Recipe[]) => this.recipes = recipes
    );
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
