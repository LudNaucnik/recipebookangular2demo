import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { Recipe } from "./recipe";
import { Ingredient } from "../shared/ingredient";
import { Subscription } from 'rxjs/Rx';
import { AuthService } from '../shared/auth.service';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class RecipeService {

  recipeChanged = new EventEmitter<Recipe[]>();

  private recipes: Recipe[] = [];

  // dummy data for testing purposes
  // private recipes: Recipe[] = [
  //   new Recipe('Cake', 'Tasty', 'http://homecookingadventure.com/images/recipes/Chocolate_Mirror_Cake_main.jpg', [new Ingredient('Cokolade', 3), new Ingredient('Flour', 2)]),
  //   new Recipe('Burek', 'Meso', 'http://cdn.yemek.com/mncrop/940/625/uploads/2015/06/kol-boregi-tarifi.jpg', [new Ingredient('Meat', 3), new Ingredient('Kori', 2)])
  // ];

  constructor(private http: Http, private authService: AuthService) { }


  getRecipes() {
    return this.recipes;
  }

  getRecipe(id: number) {
    return this.recipes[id];
  }

  deleteRecipe(recipe: Recipe) {
    this.recipes.splice(this.recipes.indexOf(recipe), 1);
    this.storeData();
    this.recipeChanged.emit(this.recipes);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.storeData();
    this.recipeChanged.emit(this.recipes);
  }

  editRecipe(oldRecipe: Recipe, newRecipe: Recipe) {
    this.recipes[this.recipes.indexOf(oldRecipe)] = newRecipe;
    this.storeData();
    this.recipeChanged.emit(this.recipes);
  }

  storeData() {
    this.authService.getCurrentToken()
      .then((idToken) => {
        const body = JSON.stringify(this.recipes);
        const headers = new Headers({
          'Content-Type': 'application/json'
        });
        this.http.put('https://recipe-book-7cce4.firebaseio.com/recipes.json?auth=' + idToken, body, { headers: headers }).subscribe(
          // error => console.log(error)
        );
      }).catch(function (error) {
        console.log(error);
      });
  }

  fetchData() {
    this.authService.getCurrentToken()
      .then((idToken) => {
        return this.http.get('https://recipe-book-7cce4.firebaseio.com/recipes.json?auth=' + idToken)
          .map((response: Response) => response.json())
          .subscribe((data: Recipe[]) => {
            if (data) {
              this.recipes = data;
              this.recipeChanged.emit(this.recipes);
            } else {
              this.recipes = [];
            }
          }
          );
      }).catch(function (error) {
        console.log(error);
      });


    // code without import
    // return this.http.get('https://recipe-book-7cce4.firebaseio.com/recipes.json')
    //   .subscribe((response: Response) => this.recipes = response.json());
  }
}
