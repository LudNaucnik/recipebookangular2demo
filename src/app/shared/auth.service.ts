import { User } from "./user.interface";
import { EventEmitter, Injectable } from "@angular/core";
import { Router } from "@angular/router";

declare var firebase: any;

@Injectable()
export class AuthService {

    constructor(private router: Router) { }

    errorEmited = new EventEmitter<string>();

    singUpUser(user: User) {
        firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
            .then((data) => {
                this.router.navigate(['recipes'])
            }).catch((error: any) => {
                this.errorEmited.emit(error.message);
            });

    }

    singInUser(user: User) {
        firebase.auth().signInWithEmailAndPassword(user.email, user.password)
            .then((data) => {
                this.router.navigate(['recipes'])
            }).catch((error: any) => {
                this.errorEmited.emit(error.message);
            });
    }

    isAuthenticated() {
        var user = firebase.auth().currentUser;
        if (user) {
            return true;
        } else {
            return false;
        }
    }

    getCurrentToken() {
        if (firebase.auth().currentUser) {
            return firebase.auth().currentUser.getIdToken();
        }
    }

    logOut() {
        firebase.auth().signOut()
            .catch(function (error) {
                console.log(error);
            });
        this.router.navigate(['sign-in']);
    }
}